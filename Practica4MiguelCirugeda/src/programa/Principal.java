package programa;

import java.util.Scanner;

import clases.Reservas;

public class Principal {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int maxClientes=5;
		Reservas restaurante=new Reservas(maxClientes);
		/**
		 * Creo un menu con 7 opciones mediante un do while con la condicion de que el numero de opciones sean diferentes a 7
		 *  y un switch con 7 opciones y un default
		 */
		int opcion;
		do {
			System.out.println("Elige una opcion de este menu");
			System.out.println("_________________________________________");
			System.out.println("1.- Alta de un cliente");
			System.out.println("2.- Busqueda de un cliente");
			System.out.println("3.- Listado de un cliente");
			System.out.println("4.- Eliminar un cliente");
			System.out.println("5.- Cambiar un cliente");
			System.out.println("6.- Listar cliente por apellidos");
			System.out.println("7.- Salir");
			System.out.println("__________________________________________");
			opcion=input.nextInt();
			input.nextLine();
			switch(opcion) {
			/**
			 * En este paso realizo el alta del cliente mediante escaner, luego se almacenara en el array restaurante
			 */
			case 1:
				System.out.println("Ha elegido opcion 1.- Alta de un cliente");
				System.out.println("Dame un nombre");
				String nombreCliente=input.nextLine();
				System.out.println("Dame un apellido");
				String apellidoCliente=input.nextLine();
				System.out.println("Dame un numero de telefono");
				int numeroTelefono=input.nextInt();
				System.out.println("Dame una hora");
				double hora=input.nextDouble();
				restaurante.altaCliente(nombreCliente, apellidoCliente, numeroTelefono, hora);
				
				break;
				/**
				 * Este caso me permite buscar un cliente y almacenarlo en el array
				 */
			case 2:
				System.out.println("Ha elegido opcion 2.- Busqueda de un cliente");
				System.out.println();
				System.out.println("Dame un nombre de busqueda");
				nombreCliente=input.nextLine();
				System.out.println(restaurante.buscarClientes(nombreCliente));
				System.out.println();
				break;
				
				/**
				 * En este caso adquiero todos los clientes almacenados y los muestro mediante el metodo listarClientes
				 */
			case 3:
				System.out.println("Ha elegido opcion 3.- Listado de un cliente");
				System.out.println();
				restaurante.listarClientes();
				System.out.println();
				break;
				
				/**
				 * En este caso elimino un cliente almacenado en el array
				 */
			case 4:
				System.out.println("Ha elegido opcion 4.- Eliminar un cliente");
				System.out.println("Dame el cliente a eliminar");
				nombreCliente=input.nextLine();
				System.out.println("Elimino "+ nombreCliente);
				restaurante.elminiarClientes(nombreCliente);
				restaurante.listarClientes();
				
				break;
				/**
				 * En este caso cambio un cliente por otro
				 */
			case 5:
				System.out.println("Ha elegido opcion 5.- Cambiar un cliente");
				System.out.println();
				System.out.println("Dame un cliente");
				String nombreCliente1=input.nextLine();
				System.out.println("Dame el cliente por el que lo quieres cambiar");
				String nombreCliente2=input.nextLine();
				restaurante.cambiarCliente(nombreCliente1, nombreCliente2);
				restaurante.listarClientes();
				break;
				/**
				 * Listo clientes por apellidos previamente almacenados
				 */
			case 6:
				System.out.println("Ha elegido opcion 6.- Listar cliente por apellidos");
				System.out.println();
				System.out.println("Dame el apellido de un cliente");
				apellidoCliente=input.nextLine();
				restaurante.listarClientesPorApellido(apellidoCliente);
				
				
				break;
			case 7:
				System.out.println("Ha elegido salir");
				break;
				/**
				 * Si la opcion es distinta de 7 me saltara opcion no contemplada y me devolvera el menu
				 */
			default:
				System.out.println("Opcion no contemplada");
			
				
			}
		}while(opcion!=7);
		
		
		input.close();
	}
	
	
}
