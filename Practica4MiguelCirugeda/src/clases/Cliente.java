package clases;

public class Cliente {
	/**
	 * Estos son los atributos de la clase Cliente
	 */

	private String nombreCliente;
	private String apellidoCliente;
	private int numeroTelefono;
	private double hora;
	
	
	/**
	 *
	 * @return devuelve nombreCliente
	 */
	
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 *
	 * @param nombreCliente nombreCliente
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * 
	 * @return devuelve apellidoCliente
	 */
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	/**
	 * 
	 * @param apellidoCliente apellidoCliente
	 */
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	/**
	 * 
	 * @return devuelve numeroTelefono
	 */
	public int getNumeroTelefono() {
		return numeroTelefono;
	}
	/**
	 * 
	 * @param numeroTelefono parametro numeroTelefono
	 */
	public void setNumeroTelefono(int numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	/**
	 * 
	 * @return devuelve la hora
	 */
	public double getHora() {
		return hora;
	}
	/**
	 * 
	 * @param hora parametro hora
	 */
	public void setHora(double hora) {
		this.hora = hora;
	}
	/**
	 * Con el metodo toString se me muestra toda la informacion en texto
	 */
	@Override
	public String toString() {
		return "Clase1 [nombreCliente=" + nombreCliente + ", apellidoCliente=" + apellidoCliente + ", numeroTelefono="
				+ numeroTelefono + ", hora=" + hora + "]";
	}


}
